import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
import matplotlib.pyplot as plt

# Load your dataset
# dataset = pd.read_csv('path_to_your_dataset.csv')

# Assuming 'X' to be the features and 'y' to be the target variable
# X = dataset.drop('target_column_name', axis=1)
# y = dataset['target_column_name']

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Scale the features
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Build the model
model = Sequential([
    Dense(64, activation='relu', input_shape=(X_train_scaled.shape[1],)),
    Dense(64, activation='relu'),
    Dense(1)  # Single output neuron for regression
])

model.compile(optimizer='adam', loss='mse', metrics=['mae'])

# Train the model
history = model.fit(X_train_scaled, y_train, validation_split=0.2, epochs=100, batch_size=32)

# Evaluate the model
test_loss, test_mae = model.evaluate(X_test_scaled, y_test)
print(f'Test MAE: {test_mae}')

# Plot the training and validation loss
plt.plot(history.history['loss'], label='Training Loss')
plt.plot(history.history['val_loss'], label='Validation Loss')
plt.title('Training and Validation Loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.show()



#X_encoded = pd.get_dummies(X, drop_first=True) //for 2.b
#//2.c
## Build a more complex model
#model_complex = Sequential([
#    Dense(128, activation='relu', input_shape=(X_train_scaled.shape[1],)),
#    Dense(128, activation='relu'),
#    Dense(64, activation='relu'),
#    Dense(1)  # Single output neuron for regression
#])

# Compile, train, and evaluate this model as before
